#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

static const char PIN_PWM_1[] = "18";

/*static char PIN_PWM_2 = "13";
static const PIN_3 = "27";
static const PIN_4 = "22";
static const PIN_5 = "23";
static const PIN_6 = "25";*/

static int write_pin(char *pin){
    printf("%s", pin);
    int fd = open("/sys/class/gpio/export", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/export");
        exit(1);
    }

    if (write(fd, pin, 2) != 2) {
        perror("Error writing to /sys/class/gpio/export");
        exit(1);
    }

    close(fd);

    fd = open("/sys/class/gpio/gpio%s/direction", pin, O_WRONLY);
    if (fd == -1) {
        fprintf(stderr, "error opening /sys/class/gpio/gpio%s/direction", pin);
        perror("");

        exit(1);
    }

    if (write(fd, "out", 3) != 3) {
        fprintf(stderr, "error with writing to /sys/class/gpio/gpio%s/direction", pin);
        perror("");
        exit(1);
    }

    close(fd);

    fd = open("/sys/class/gpio/gpio%s/value", pin, O_WRONLY);
    if (fd == -1) {
        fprintf(stderr, "error opening /sys/class/gpio/gpio%s/value", pin);
        perror("");
        exit(1);
    }

    if (write(fd, "1", 1) != 1) {
        fprintf(stderr, "error with writing to /sys/class/gpio/gpio%s/value", pin);
        perror("");
        exit(1);
        }

        return 0;
}

static int unwrite_pin(char *pin){
    int fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/unexport");
        exit(1);
    }

    if (write(fd, pin, 2) != 2) {
        perror("Error writing to /sys/class/gpio/unexport");
        exit(1);
    }

    close(fd);

    return 0;
}

static int write_pwm_config( char *fname, char *value )
{
  int fd_pwm, ret, length;
  char fpath[128];
  if (fname==NULL || value==NULL) {
    return -1;
  }
  snprintf( fpath, sizeof(fpath), "/sys/class/pwm/pwmchip0/%s", fname);
  length = strlen( fpath );
  if (length >= sizeof(fpath)) {
    fprintf( stderr, "path %s too long\n", fname);
    return -1;
  }
  fd_pwm = open( fpath, O_WRONLY );
  if (fd_pwm<0) {
    perror( fpath );
    return -1;
  }
  ret = write(fd_pwm, value, strlen(value));
  if (ret<0) {
    perror( value );
    close( fd_pwm );
    return -2;
  }
  close( fd_pwm );
  return 0;
}



static void forward(){
    write_pin(PIN_PWM_1);
    //write_pin(PIN_4);
}

static void stop(){
    unwrite_pin(PIN_PWM_1);
    //unwrite_pin(PIN_4);
    //unwrite_pin(PIN_5);
    //unwrite_pin(PIN_6);
    //unwrite_pin(PIN_PWM_1);
    //unwrite_pin(PIN_PWM_2);
}

int main( int argc, char **argv )
{
    forward();
    usleep(5000000);
    stop();
  /*int ret;
  ret = write_pwm_config("unexport","0");
  if (ret) return ret;
  ret = write_pwm_config("export","0");
  if (ret) return ret;
  ret = write_pwm_config("pwm0/period", "500000000");
  if (ret) return ret;
  ret = write_pwm_config("pwm0/duty_cycle", "400000000");
  if (ret) return ret;
  ret = write_pwm_config("pwm0/enable", "1");
  if (ret) return ret;
  return 0;*/
}
