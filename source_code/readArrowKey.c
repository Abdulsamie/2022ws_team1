// to translate this code: gcc tmp.c  -o tmpo && ./tmpo
#include<stdio.h>
#include <curses.h>
#include<stdlib.h>

// https://stackoverflow.com/questions/1798511/how-to-avoid-pressing-enter-with-getchar-for-reading-a-single-character-only
int main(void){
  int c;
  /* use system call to make terminal send all keystrokes directly to stdin */
  system ("/bin/stty raw");

  //unsigned char11 c =
while((c=getchar())!= 'q') { // to end the programm pleas put q

     /* type a period to break out of the loop, since CTRL-D won't work raw */
     	// to read letter keys from stdin
     	if(c == 'w'){
     		printf("\rLetter:\tw-taste\n\r");
     	}
     	if(c == 'd'){
     		printf("\rLetter:\td-taste\n\r");
     	}
     	if(c == 's'){
     		printf("\rLetter:\ts-taste\n\r");
     	}
     	if(c == 'a'){
     		printf("\rLetter:\tw-taste\n\r");
     	}
     	

     	// to read arrow keys from stdin
     	if(c == '['){
    		c= getchar();
     		if(c== 'A'){
     			printf("\rLetter:\tUp-taste\n\r");
     		}
     		else if(c== 'D'){
     			printf("\rLetter:\tLeft-taste\n\r");
     		}
     		else if(c== 'C'){
     			printf("\rLetter:\tRight-taste\n\r");
     		}
     		else if(c== 'B'){
     			printf("\rLetter:\tDown-taste\n\r");
     		}
     	}
}
  /* use system call to set terminal behaviour to more normal behaviour */

 system ("/bin/stty cooked");
  
  return 0;
}




/*
to translate this code: gcc -Wall tmp.c -lcurses -ocurses-test -o tmpo && ./tmpo  
int main (int argc, char **argv){
	for(;;){
  		int c = getch();
		if(ERR == c){
			continue;
		}
		if(KEY_UP == c)
		{
			continue;
		}
		if(c == 72){
			printf("Up-Key");
		}
		else if(c==75){
			printf("Left");
		}
  		printf("Char: 0x%x\n", c);
		refresh();
}
  return 0;
}
// https://stackoverflow.com/questions/58086775/how-to-listen-for-arrow-key-press-in-c
*/
