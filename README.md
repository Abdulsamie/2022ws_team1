# 2022WS_team1

This is the Repository for the Omniwheel Project by Group A. This project is done for C71 CSE course.

## Structure

All the technical requirements are listed under [Requirements](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/requirements_management/requirements).

The Repository itself is used for technical artifacts (files such as source code, drawings, models etc.). Our source code can be found in the directory [source_code](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/tree/main/source_code). 

Documentation (explanations, concepts...) is stored in the project [Wiki](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/wikis/home). The project wiki shall is structured according to project phases so you can find information easily.

The presentations are stored in the [documentation](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/tree/main/documentation) folder in the repository.
